<?php

  Yii::import('zii.widgets.CBreadcrumbs');

  class PBreadcrumbsWidget extends CBreadcrumbs {

    public $homeLink = false;
    public $separator = ' › ';

    public function run() {
      $items = array();

      if ($this->homeLink !== false) {
        $items = array_merge($this->homeLink,$items);
      }

      if ($this->links) {
        $items = array_merge($items, $this->links);
      }

      $this->render('breadcrumbs', array(
        'items' => $items
      ));
    }
  }