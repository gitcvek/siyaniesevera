<?php
  $keys = array_keys($items);
  $lastItem = array_pop($keys);
  if($lastItem) {
    array_pop($items);
  } else {
    $lastItem = array_pop($items);
  }
?>
<div class="b-breadcrumbs">
  <?php
    foreach ($items as $label => $link) {
      echo CHtml::link($label, $link, array('class' => 'b-breadcrumbs_link'));
      echo $this->separator;
    }
    echo $lastItem;
  ?>
</div>