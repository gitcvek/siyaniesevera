<?php
/**
 * Created by PhpStorm.
 * User: Cranky4
 * Date: 02.09.14
 * Time: 10:43
 */

class PFeedback extends Feedback {

  public function rules() {
    return CMap::mergeArray(
      parent::rules(),
      array(
        array('phone','required')
      )
    );
  }

} 