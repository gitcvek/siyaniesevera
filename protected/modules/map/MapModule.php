<?php

class MapModule extends DaWebModuleAbstract {

	protected $_urlRules = array(
	  'map/<id:\d+>' => 'map/default/view',
	  'map' => 'map/default/index',
	);
	
	public function init() {
		$this->setImport(array(
			$this->id.'.models.*',
			$this->id.'.components.*',
		));
	}

}
