<?php

class DefaultController extends Controller {
  public $urlAlias = 'map';
  /**
   * @var Settlement $model
   */
  public function actionIndex() {

    $models = Settlement::model()->findAll();

    $geocode = array();
    foreach ($models as $model) {
      $_data['title'] = $model->name;
      $_data['coords'] = $model->coords;
      $_data['image'] = $model->getImage(null,200,"_map");
      $_data['descr'] = $model->short_description;
      $_data['link'] = $model->getUrl();

      $geocode[] = $_data;
    }

    $this->render('index', array(
      'geocode' => CJSON::encode($geocode),
    ));
	}

  public function actionView($id) {
    $model = $this->loadModelOr404('Settlement',$id);

    $this->render('view', array(
      'model' => $model
    ));
  }
}