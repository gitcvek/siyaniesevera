<?php

/**
 * Модель для таблицы "pr_settlement".
 *
 * The followings are the available columns in table 'pr_settlement':
 * @property integer $id_settlement
 * @property string $name
 * @property integer $image
 * @property string $short_description
 * @property string $description
 * @property string $coords
 * @property integer $visible
 *
 * The followings are the available model relations:
 * @property File $imageFile
 */
class Settlement extends DaActiveRecord implements ISearchable {

  const ID_OBJECT = 'project-interaktivaniya-karta';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Settlement the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pr_settlement';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('name, coords', 'required'),
      array('image, visible', 'numerical', 'integerOnly'=>true),
      array('name, coords', 'length', 'max'=>255),
      array('short_description, description', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'imageFile' => array(self::BELONGS_TO, 'File', 'image'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_settlement' => 'ID',
      'name' => 'Название',
      'image' => 'Фото',
      'short_description' => 'Краткое описание',
      'description' => 'Описание',
      'coords' => 'Координаты',
      'visible' => 'Видимость',
    );
  }

  public function defaultScope() {
    return array(
      'condition' => 'visible = 1',
    );
  }

  public function getImage($w=600,$h=400,$prefix="_p") {
    if($this->imageFile && file_exists($this->imageFile->getFilePath(true))) {
      return "/".$this->imageFile->getPreview($w,$h,$prefix)->getFilePath();
    }
    return false;
  }

  public function getUrl() {
    return Yii::app()->createUrl('map/default/view', array('id' => $this->primaryKey));
  }

  public function getSearchTitle() {
    return $this->name;
  }

  public function getSearchUrl() {
    return $this->getUrl();
  }
}