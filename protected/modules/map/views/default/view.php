<?php
  /**
   * @var Settlement $model
   */
  $this->pageTitle = $this->caption = $model->name;
  $this->breadcrumbs = array(
    'Карта Коми' => Yii::app()->createUrl('map/default/index'),
    $model->name
  )
?>
<div class="b-settlement">
<!--  <div class="photo">-->
<!--    --><?php //echo CHtml::image($model->getImage(),$model->name, array('class' => 'image image-thumbsnail'));?>
<!--  </div>-->
  <div class="descr">
    <?php echo $model->description; ?>
  </div>
</div>