<?php

class TourModule extends DaWebModuleAbstract {
  public $name = "Туры";

	protected $_urlRules = array(
	  'tour/<id:\d+>' => 'tour/default/view',
	  'tour' => 'tour/default/index',
    'tour-category-<id:\d+>' => 'tour/default/category/'
	);
	
	public function init() {
		$this->setImport(array(
			$this->id.'.models.*',
			$this->id.'.components.*',
		));
	}

}
