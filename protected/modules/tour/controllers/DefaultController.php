<?php

class DefaultController extends Controller {
  public $urlAlias = 'tours';

	public function actionIndex() {
    $models = Tour::model()->findAll();

		$this->render('index', array(
      'models' => $models,
    ));
	}

  public function actionView($id) {
    $model = $this->loadModelOr404('Tour',$id);

    $this->render('view', array(
      'model' => $model,
    ));
  }

  public function actionCategory($id) {
    $models = Tour::model()->findAllByAttributes(array('id_tour_category' => $id));

    if(count($models) == 0)
      throw new CHttpException(404);

    $this->render('index', array(
      'models' => $models
    ))
;  }
}