<?php

/**
 * Модель для таблицы "pr_tour".
 *
 * The followings are the available columns in table 'pr_tour':
 * @property integer $id_tour
 * @property string $title
 * @property string $description
 * @property string $date
 * @property integer $price
 * @property integer $id_photogallery
 * @property integer $visible
 * @property string $short_description
 * @property integer $id_image
 * @property integer $id_tour_category
 *
 * The followings are the available model relations:
 * @property File $imageFile
 * @property Photogallery $photogallery
 */
class Tour extends DaActiveRecord implements ISearchable {

  const ID_OBJECT = 'project-tur';
  const ID_CATEGORY_REFERENCE = 'project-reference-kategoriya-tura';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Tour the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pr_tour';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('title, id_image, id_tour_category', 'required'),
      array('price, id_photogallery, visible, id_image, id_tour_category', 'numerical', 'integerOnly'=>true),
      array('title', 'length', 'max'=>255),
      array('date', 'length', 'max'=>10),
      array('description, short_description', 'safe'),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'imageFile' => array(self::BELONGS_TO, 'File', 'id_image'),
      'photogallery' => array(self::BELONGS_TO, 'Photogallery', 'id_photogallery'),
    );
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_tour' => 'ID',
      'title' => 'Название',
      'description' => 'Описание',
      'date' => 'Дата создания',
      'price' => 'Цена',
      'id_photogallery' => 'Фотогалерея',
      'visible' => 'Видимость',
      'short_description' => 'Краткое описание',
      'id_image' => 'Изображение',
      'id_tour_category' => 'Категория',
    );
  }


  public function getDate(){
    if($this->date) {
      return Yii::app()->dateFormatter->format('d MMMM yyyy',$this->date);
    }
    return false;
  }

  public function defaultScope() {
    return array(
      'condition' => 't.visible = 1',
    );
  }

  public function getImage($w=600,$h=400,$prefix="_p") {
    if($this->imageFile && file_exists($this->imageFile->getFilePath(true))) {
      return "/".$this->imageFile->getPreview($w,$h,$prefix)->getFilePath();
    }
    return false;
  }

  public function getUrl() {
    return Yii::app()->createUrl('tour/default/view', array('id' => $this->primaryKey));
  }

  public function getSearchTitle() {
    return $this->title;
  }

  public function getSearchUrl() {
    return $this->getUrl();
  }

  public function getFormattedPrice() {
    if($this->price)
      return $this->price.' р.';
    return false;
  }
}