<div class="b-tour-categories">
    <ul class="items nav nav-tabs">
        <?php $active = "active"; ?>
        <?php foreach ($data as $key => $value): ?>
            <?php //если мы находимся в этой категории, то выводим без ссылки ?>
            <?php if ($key == Yii::app()->request->getParam('id')): ?>
                <?php $active = ""; ?>
                <li class="active"><?php echo CHtml::link($value, '#', array('class' => '')); ?></li>
            <?php else: ?>
                <li><?php echo CHtml::link($value, Yii::app()->createUrl('tour/default/category', array('id' => $key)), array('class' => '')); ?></li>
            <?php endif; ?>
        <?php endforeach; ?>
        <?php //если мы находимся во всех турах, то не выводим ссылку на все ?>
        <li class="<?php echo $active; ?>"><?php echo CHtml::link('Все туры', Yii::app()->createUrl('tour/default/index'), array('class' => '')); ?></li>
    </ul>
</div>