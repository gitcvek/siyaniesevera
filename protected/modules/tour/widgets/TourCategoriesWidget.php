<?php
/**
 * Created by PhpStorm.
 * User: Cranky4
 * Date: 28.08.14
 * Time: 14:18
 */

class TourCategoriesWidget extends DaWidget {

  public function run() {
    $categories = ReferenceElement::model()->findAllByAttributes(array('id_reference' => Tour::ID_CATEGORY_REFERENCE));

    $listData = CHtml::listData($categories,'id_reference_element','value');

    if(count($categories) != 0) {
      $this->render('tourCategories', array(
        'data' => $listData,
      ));
    }

  }

} 