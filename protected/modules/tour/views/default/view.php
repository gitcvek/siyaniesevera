<?php
  /**
   * @var Tour $model
   */
  $this->breadcrumbs=array(
    'Туры' => Yii::app()->createUrl('tour/default/index'),
    $model->title,
  );
?>

<?php $this->pageTitle = $this->caption = $model->title;?>

<div class="b-tour">
    <div class="item">
        <?php if($model->price):?>
          <div class="price"><?php echo $model->getFormattedPrice();?></div>
        <?php endif; ?>
        <div class="desc clearfix"><?php echo $model->description; ?></div>
        <div class="date"><?php echo $model->getDate(); ?></div>
        <div class="photos">
            <h1>Фотогалерея</h1>
            <?php if ($model->photogallery): ?>
                <?php $this->widget('PhotogalleryWidget', array("model" => $model->photogallery)); ?>
            <?php endif; ?>
        </div>
    </div>
</div>