<?php $this->pageTitle = $this->caption = 'Туры';?>
<?php
  /**
   * @var Tour $model
   */
  $this->breadcrumbs=array(
    $this->module->name,
  );
?>
<?php $this->widget('tour.widgets.TourCategoriesWidget');?>
<div class="b-tours">
    <div class="items row">
        <?php foreach ($models as $model): ?>
            <div class="item col-sm-4">
                <div class="item-inner">
                    <div class="image">
                        <?php echo CHtml::link(CHtml::image($model->getImage(340, 250, '_list'), $model->title),$model->getUrl());?>
                    </div>
                    <div class="title clearfix"><?php echo CHtml::link($model->title, $model->getUrl(), array('class' => 'link')); ?></div>
                    <div class="description clearfix"><?php echo $model->short_description; ?></div>
                    <div class="date"><?php echo $model->getDate(); ?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<script>
    matchHeight ( $ ( '.b-tours .item .description' ),3 );
    matchHeight ( $ ( '.b-tours .item .title' ),3 );
</script>
