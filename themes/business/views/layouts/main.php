<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="content-language" content="ru" > <?php // TODO - в будущем генетить автоматом ?>
  <meta name="google-translate-customization" content="1250ed8209d11cc-9bd2e400f624c36e-g39b12e1e7c4dc662-1d">
<?php
  //Регистрируем файлы скриптов в <head>
  if (YII_DEBUG) {
    Yii::app()->assetManager->publish(YII_PATH.'/web/js/source', false, -1, true);
  }
  Yii::app()->clientScript->registerCoreScript('jquery');
  //$this->registerJsFile('bootstrap.min.js', 'ygin.assets.bootstrap.js');
  $this->registerJsFile('modernizr-2.6.1-respond-1.1.0.min.js', 'ygin.assets.js');
  Yii::app()->clientScript->registerCssFile('/themes/business/dist/css/bootstrap.min.css');
  Yii::app()->clientScript->registerScriptFile('/themes/business/dist/js/bootstrap.min.js', CClientScript::POS_HEAD);
  Yii::app()->clientScript->registerScriptFile('/themes/business/js/js.js', CClientScript::POS_HEAD);
  Yii::app()->clientScript->registerScriptFile('/themes/business/js/match-height.js', CClientScript::POS_HEAD);

  Yii::app()->clientScript->registerScript('menu.init', "$('.dropdown-toggle').dropdown();", CClientScript::POS_READY);

  //$this->registerCssFile('bootstrap.min.css', 'application.assets.bootstrap.css');

  //$this->registerCssFile('bootstrap-responsive.min.css', 'application.assets.bootstrap.css');
  $ass = Yii::getPathOfAlias('webroot.themes.business.dist.fonts').DIRECTORY_SEPARATOR;
    Yii::app()->clientScript->addDependResource('bootstrap.min.css', array(
        $ass.'glyphicons-halflings-regular.ttf' => '../fonts/',
        $ass.'glyphicons-halflings-regular.svg' => '../fonts/',
        $ass.'glyphicons-halflings-regular.eot' => '../fonts/',
        $ass.'glyphicons-halflings-regular.woff' => '../fonts/',
    ));

  Yii::app()->clientScript->registerCssFile('/themes/business/css/content.css');
  Yii::app()->clientScript->registerCssFile('/themes/business/css/page.css');
?>
  <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<body>
  <div id="wrap" class="container">
    <div id="head" class="row">
<?php if (Yii::app()->request->url == "/"){ ?>
      <div class="logo"><img border="0" alt="Название компании - На главную" src="/themes/business/gfx/header-logo2.png"></div>
<?php } else { ?>
      <a href="/" title="Главная страница" class="logo col-sm-2"><img src="/themes/business/gfx/header-logo2.png" alt="Логотип компании"></a>
<?php }?>
      <div class="cname col-sm-7">
      <p>
      	Филиал Государственного автономного образовательного учреждения Республики Коми дополнительного образования детей
      	<br>
      	<strong>&laquo;Республиканский центр детско-юношеского спорта и туризма&raquo;</strong>
      </p>
      <?/*<span class="company-type">Туристский поезд</span> */?>
        <p class="company-name">«ТурЦентр» <span>Сыктывкар</span></p> 
      </div>
      <div class="tright col-sm-3 pull-right">
        <div class="numbers">
          <p>
          <div class="header_contacts-row phone">+7 (8212) 336-907</div>
          <div class="header_contacts-row mail"><a href="mailto:Turcentr.tur@mail.ru">Turcentr.tur@mail.ru</a></div>
          </p>
        </div>
          <?php $this->widget('application.widgets.translateWidget');?>
        <?php 
		if (Yii::app()->hasModule('search')) {
		  $this->widget('SearchWidget');
		}
        ?>
      </div>
    </div>
    <div class="b-menu-top navbar">
      <div class="nav-collapse">
<?php

$this->widget('MenuWidget', array(
  'rootItem' => Yii::app()->menu->all,
  'htmlOptions' => array('class' => 'b-menu-top_items nav nav-pills'), // корневой ul
  'submenuHtmlOptions' => array('class' => 'dropdown-menu'), // все ul кроме корневого
  'activeCssClass' => 'active', // активный li
  'activateParents' => 'true', // добавлять активность не только для конечного раздела, но и для всех родителей
  //'labelTemplate' => '{label}', // шаблон для подписи
  'labelDropDownTemplate' => '{label} <b class="caret"></b>', // шаблон для подписи разделов, которых есть потомки
  //'linkOptions' => array(), // атрибуты для ссылок
  'linkDropDownOptions' => array('data-target' => '#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
  //'itemOptions' => array(), // атрибуты для li
  'itemDropDownOptions' => array('class' => 'dropdown'),  // атрибуты для li разделов, у которых есть потомки
  'maxChildLevel' => 1,
  'encodeLabel' => false,
));

?>
      </div>
    </div>

<?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_TOP)); ?>

<?php // + Главный блок ?>
    <div id="main">
      <div id="container" class="row">
<?php

$column1 = 0;
$column2 = 12;
$column3 = 0;

if (Yii::app()->menu->current != null) {
  $column1 = 3;
  $column2 = 6;
  $column3 = 3;
  
  if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_LEFT) == 0) {$column1 = 0; $column3 = 4;}
  if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_RIGHT) == 0) {$column3 = 0; $column1 = $column1*4/3;}
  $column2 = 12 - $column1 - $column3;
}

?>
        <?php if ($column1 > 0): // левая колонка ?>
        <div id="sidebarLeft" class="col-sm-<?php echo $column1; ?>">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_LEFT)); ?>
        </div>
        <?php endif ?>
        
        <div id="content" class="col-sm-<?php echo $column2; ?>">
          <div class="page-header">
            <h1><?php echo $this->caption; ?></h1>
          </div>
          <?php if (isset($this->breadcrumbs) && Yii::app()->request->url != "/"): // Цепочка навигации ?>
          <?php $this->widget('application.widgets.PBreadcrumbsWidget', array(
            'homeLink' => array('Главная' => Yii::app()->homeUrl),
            'links' => $this->breadcrumbs,
          )); ?>
          <?php endif ?>

          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_CONTENT_TOP)); ?>
          <div class="cContent">
            <?php echo $content; ?>
          </div>
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_BOTTOM)); ?>

        </div>

        <?php if ($column3 > 0): // левая колонка ?>
        <div id="sidebarRight" class="col-sm-<?php echo $column3; ?>">
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
        </div>
        <?php endif ?>

      </div>
<?php //Тут возможно какие-нить модули снизу ?>
      <div class="clr"></div>
    </div>
<?php // - Главный блок ?>
    
  </div>


  <div id="footer" class="container">
      <div class="container">
        <div class="row">
          <div class="col-sm-10">
            <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_FOOTER)); ?>
          </div>
          <div class="col-sm-2">
            <div id="cvek"><a class="pull-right" title="создать сайт в Цифровом веке" href="http://cvek.ru">Создание сайта — веб-студия &laquo;Цифровой век&raquo;</a></div>
          </div>
        </div>
      </div>
  </div>

</body>
</html>